#! /usr/bin/env python

import datetime as dt
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

path = './input'

donations = pd.read_csv(path + '/Donations.csv',
                        names=columns,
                        header=0,
                        dtype=dtypes,
                        infer_datetime_format=True,
                        parse_dates=True,
                        low_memory=False)

donors = pd.read_csv(path + '/Donors.csv',
                     header=0,
                     infer_datetime_format=True,
                     parse_dates=True,
                     low_memory=False)

#projects = pd.read_csv(path + '/Projects.csv',
#                        header=0,
#                        infer_datetime_format=True,
#                        parse_dates=True,
#                        low_memory=False)
#resources = pd.read_csv(path + '/Resources.csv',
#                        header=0,
#                        infer_datetime_format=True,
#                        parse_dates=True,
#                        low_memory=False)
#schools = pd.read_csv(path + '/Schools.csv',
#                        names=columns,
#                        header=0,
#                        dtype=dtypes,
#                        infer_datetime_format=True,
#                        parse_dates=True,
#                        low_memory=False)
#teachers = pd.read_csv(path + '/Teachers.csv',
#                        names=columns,
#                        header=0,
#                        dtype=dtypes,
#                        infer_datetime_format=True,
#                        parse_dates=True,
#                        low_memory=False)

#Note the as_index option will enable the datasets to be joined. The default behaviour of the groupby method is to make the groupby variable an index.
df_by_donor = donations[['Donor ID',
                         'Donation ID',
                         'Donation Amount',
                         'Donation Received Date']].groupby('Donor ID',
                                                            as_index=False).agg({'Donation ID': 'count', 'Donation Received Date': 'max', 'Donation Amount': ['min', 'max', 'mean', 'sum']})

if __name__ == "__main__":
    pass
